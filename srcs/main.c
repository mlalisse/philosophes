/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 13:17:06 by mlalisse          #+#    #+#             */
/*   Updated: 2014/05/11 21:00:46 by jwoodrow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

t_philosopher	g_philo[7];

long long		hungriest(long long a, long long b)
{
	long long	i;
	long long	min_i;
	int			min;

	i = 0;
	min = MAX_LIFE;
	while (i < 7)
	{
		if ((a == -1 || (i != a && i != RIGHT(a) && i != LEFT(a)))
			&& (b == -1 || (i != b && i != RIGHT(b) && i != LEFT(b)))
			&& g_philo[i].life < min)
		{
			min_i = i;
			min = g_philo[i].life;
		}
		i++;
	}
	return (min_i);
}

void			*philosopher(void *vn)
{
	long long	n;
	long long	min_i[3];

	n = (long long) vn;
	while (1)
	{
		min_i[0] = hungriest(-1, -1);
		min_i[1] = hungriest(min_i[0], -1);
		if (n == min_i[0] || n == min_i[1] || n == hungriest(min_i[0], min_i[1]))
		{
			pthread_mutex_lock(&g_philo[n].stick);
			pthread_mutex_lock(&g_philo[LEFT(n)].stick);
			ft_printf("Philosopher %d (%d) eat.\n", (int) n, g_philo[n].life);
			g_philo[n].life = MAX_LIFE;
			usleep(EAT_TIME * 1000 * 1000);
			pthread_mutex_unlock(&g_philo[n].stick);
			pthread_mutex_unlock(&g_philo[LEFT(n)].stick);
			g_philo[n].state = STATE_EAT;
			continue ;
		}
		ft_printf("Philosopher %d (%d) rest.\n", (int) n, g_philo[n].life);
		usleep(REST_TIME * 1000 * 1000);
		g_philo[n].state = STATE_REST;
	}
	return (NULL);
}

void			philopsophing(void)
{
	int		i;
	int		t;

	t = 0;
	while (t++ < TIMEOUT)
	{
		write(1, "tick\n", 5);
		usleep(1000 * 1000);
		i = 0;
		while (i < 7)
		{
			if (g_philo[i].life <= 0)
			{
				ft_printf("Philosopher %d died!\n", i);
				return ;
			}
			g_philo[i++].life--;
		}
	}
	ft_printf("Now, it is time... To DAAAAAAAANCE !!!\n");
}

int				main(void)
{
	long long	i;
	pthread_t	threads[7];

	i = 0;
	while (i < 7)
	{
		g_philo[i].life = MAX_LIFE;
		g_philo[i].state = STATE_UNDEF;
		pthread_mutex_init(&g_philo[i].stick, NULL);
		if (pthread_create(threads + i, NULL, philosopher, (void *) i) != 0)
			return (-1);
		i++;
	}
	philopsophing();
	i = 0;
	while (i < 7)
	{
		pthread_mutex_unlock(&g_philo[i].stick);
		pthread_mutex_destroy(&g_philo[i].stick);
		i++;
	}
	return (0);
}
