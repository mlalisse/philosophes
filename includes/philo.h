/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 13:22:36 by mlalisse          #+#    #+#             */
/*   Updated: 2014/05/11 20:58:40 by jwoodrow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H
# include <unistd.h>
# include <pthread.h>
# include "libftprintf.h"

# define MAX_LIFE 30
# define EAT_TIME 5
# define REST_TIME 3
# define THINK_TIME 2
# define TIMEOUT 50

# define RIGHT(i) ((i >= 6) ? 0 : i + 1)
# define LEFT(i) ((i <= 0) ? 6 : i - 1)

enum				e_state
{
	STATE_UNDEF = -1,
	STATE_REST,
	STATE_THINK,
	STATE_EAT
};

typedef struct		s_philosopher
{
	int				life;
	int				state;
	pthread_mutex_t	stick;
}					t_philosopher;

#endif
