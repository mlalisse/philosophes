#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwoodrow <jwoodrow@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/30 13:57:53 by jwoodrow          #+#    #+#              #
#    Updated: 2014/05/11 20:58:21 by jwoodrow         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = philo
LIBNAME = ftprintf
C = \033[0;36m
CFLAGS = -g3 -Wall -Werror -Wextra
SRCPATH = ./srcs/
INCLUDE = ./includes
SRCS = main.c

V = 0
SILENCE_1 :=
SILENCE_0 :=@
SILENCE = $(SILENCE_$(V))
LNAME = $(addprefix lib, $(LIBNAME)) /usr/x11/lib
CLNAME = $(addprefix -l, $(LIBNAME)) -lmlx -lXext -lX11
LIB = $(addprefix -L, $(addsuffix /, $(LNAME))) $(CLNAME)
LINCLUDE = $(addsuffix /includes/, $(LNAME)) /usr/x11/include/
CC = $(SILENCE)gcc
RM = $(SILENCE)rm -rf
MAKE = $(SILENCE)make V=$(V)
INCLUDES = -I $(INCLUDE) $(addprefix -I, $(LINCLUDE))
SRC = $(addprefix $(SRCPATH), $(SRCS))
OBJS= $(SRCS:.c=.o)
SKIP_1 :=
SKIP_0 := \033[A
SKIP = $(SKIP_$(V))
U = $(C)[$(NAME)]----->\033[0m
OBJDIR = $(addprefix obj/, $(OBJS))

all: libftprintf createdir $(NAME)

$(NAME): $(OBJDIR)
	@echo "$(U)$(C)[COMPILE:\033[1;32m DONE$(C)]\033[0m"
	@echo "$(U)$(C)[BUILD]\033[0;32m"
	$(CC) -o $(NAME) $(OBJDIR) $(CFLAGS) $(INCLUDES) $(LIB)
	@echo "$(SKIP)\033[2K"
	@echo "$(SKIP)$(U)$(C)[BUILD  :\033[1;32m DONE$(C)]\033[0m"

obj/%.o: srcs/%.c
	@echo "$(U)$(C)[COMPILE: \033[1;31m$<\033[A\033[0m"
	@echo "\033[0;32m"
	$(CC) -o $@ $(CFLAGS) $(INCLUDES) -c $<
	@echo "\033[1;31m [.working.]"
	@echo "$(SKIP)\033[2K\033[A\033[2K$(SKIP)"

.clean:
	@echo "$(U)$(C)[CLEAN]\033[0;32m"
	$(RM) $(OBJS)
	@echo "$(SKIP)$(U)$(C)[CLEAN:\033[1;32m   DONE$(C)]\033[0m"

clean: libftprintfclean .clean

fclean: libftprintffclean .clean
	@echo "$(U)$(C)[F-CLEAN]\033[0;32m"
	$(RM) $(NAME)
	$(RM) -rf obj
	@echo "$(SKIP)$(U)$(C)[F-CLEAN:\033[1;32m DONE$(C)]\033[0m"

libftprintf:
	$(MAKE) all -C libftprintf/
	@echo "$(SKIP)"

libftprintfclean:
	$(MAKE) clean -C libftprintf/
	@echo "$(SKIP)"

libftprintffclean:
	$(MAKE) fclean -C libftprintf/
	@echo "$(SKIP)"

createdir:
	@mkdir -p obj

re: fclean all

.PHONY: clean .clean fclean	libftprintf libftprintfclean libftprintffclean createdir
